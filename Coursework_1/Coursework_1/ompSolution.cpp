#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <vector>
#include <omp.h>

using namespace std;
using namespace chrono;

// Defines the data size to work on
const unsigned int SIZE = 1000;
// Used to validate the result.  This is related to the data size
const double CHECK_VALUE = 12.0;
//vector<thread> threads;

// OPEN MP

double matgen(double **a, int n, double *b)
{
	double norma = 0.0;
	int init = 1325;

	for (int i = 0; i < n; ++i)
	{
		b[i] = 0.0;
		for (int j = 0; j < n; ++j)
		{
			init = 3125 * init % 65536;
			b[i] += a[j][i] = (static_cast<double>(init)-32768.0) / 16384.0;

			if (a[j][i] > norma) norma = a[j][i];
		}
	}
	return norma;
}

int idamax(int n, double *dx, int dx_off, int incx)
{
	double dmax, dtemp;
	int ix, itemp = 0;

	itemp = 0;
	dmax = abs(dx[dx_off]);

	for (int i = 0; i < n; ++i)
	{
		dtemp = abs(dx[i + dx_off]);
		if (dtemp > dmax)
		{
			itemp = i;
			dmax = dtemp;
		}
	}

	return itemp;
}

// Scales a vector by a constant
void dscal(int n, double da, double *dx, int dx_off)
{
	int i;
	// Serialize and multiply by set da
	for (i = 0; i < n; ++i)
		dx[i + dx_off] *= da;
}

// Constant times a vector plus a vector
void daxpy(int n, double da, double *dx, int dx_off, double *dy)
{
	// OMP Optimisation
	const int steps = 8;
	unsigned int rmdr = n % steps;

	for (int i = 0; i < rmdr; ++i)
	{
		dy[i + dx_off] += da * dx[i + dx_off];
	}

#pragma omp parallel for num_threads(4)
	for (int i = rmdr; i < n; i += steps)
	{
		// change the number of calculations when changing the steps size
		dy[i + dx_off] += da * dx[i + dx_off];
		dy[i + 1 + dx_off] += da * dx[i + 1 + dx_off];
		dy[i + 2 + dx_off] += da * dx[i + 2 + dx_off];
		dy[i + 3 + dx_off] += da * dx[i + 3 + dx_off];
		dy[i + 4 + dx_off] += da * dx[i + 4 + dx_off];
		dy[i + 5 + dx_off] += da * dx[i + 5 + dx_off];
		dy[i + 6 + dx_off] += da * dx[i + 6 + dx_off];
		dy[i + 7 + dx_off] += da * dx[i + 7 + dx_off];

		//dy[i + 8 + dx_off] += da * dx[i + 8 + dx_off];
		//dy[i + 9 + dx_off] += da * dx[i + 9 + dx_off];
		//dy[i + 10 + dx_off] += da * dx[i + 10 + dx_off];
		//dy[i + 11 + dx_off] += da * dx[i + 11 + dx_off];
		//dy[i + 12 + dx_off] += da * dx[i + 12 + dx_off];
		//dy[i + 13 + dx_off] += da * dx[i + 13 + dx_off];
		//dy[i + 14 + dx_off] += da * dx[i + 14 + dx_off];
		//dy[i + 15 + dx_off] += da * dx[i + 15 + dx_off];

	}
}

// Performs Gaussian elimination with partial pivoting
int dgefa(double **a, int n, int *ipvt)
{
	// Pointers to columns being worked on
	double *col_k, *col_j;
	//	double *arrayF; //
	//	ofstream data("coljLoc.csv", ofstream::out);
	double t;
	int kp1, l;
	int nm1 = n - 1;
	int info = 0;

	if (nm1 >= 0)
	{
		for (int k = 0; k < nm1; ++k)
		{
			// Set pointer for col_k to relevant column in a
			col_k = &a[k][0];
			//	arrayF = &a[k][0];

			kp1 = k + 1;
			if (k == nm1 - 1)
				int j = 9;
			// Find pivot index
			l = idamax(n - k, col_k, k, 1) + k;
			ipvt[k] = l;

			// Zero pivot means that this column is already triangularized
			if (col_k[l] != 0)
			{
				// Check if we need to interchange
				t = col_k[l];

				if (t > 3.0)
					int j = 0;
				if (l != k)
				{
					col_k[l] = col_k[k];
					col_k[k] = t;
				}

				// Compute multipliers
				t = -1.0 / t;
				dscal(n - kp1, t, col_k, kp1);
				int j;
#pragma omp parallel num_threads(4) default(none) shared(j, a, kp1, n, l, k, col_k) private(col_j, t)
#pragma omp for
				// Row elimination with column indexing
				for (int j = kp1; j < n; ++j)
				{
					// Set pointer for col_j to relevant column in a
					col_j = &a[j][0];

					t = col_j[l];
					if (l != k)
					{
						col_j[l] = col_j[k];
						col_j[k] = t;
					}
					daxpy(n - kp1, t, col_k, kp1, col_j);
				}
			}
		}
	}

	ipvt[n - 1] = n - 1;
	if (a[n - 1][n - 1] == 0)
		info = n - 1;
	return info;
}

// Solves the system a * x = b using the factors computed in dgeco or dgefa
void dgesl(double **a, int lda, int n, int *ipvt, double *b, int job)
{
	double t;
	int k, l, nm1, kp1;

	nm1 = n - 1;

	if (job == 0)
	{
		// Solve a * x = b.  First solve l * y = b
		if (nm1 >= 1)
		{
			for (k = 0; k < nm1; ++k)
			{
				l = ipvt[k];
				t = b[l];
				if (l != k)
				{
					b[l] = b[k];
					b[k] = t;
				}
				kp1 = k + 1;
				daxpy(n - kp1, t, &a[k][0], kp1, b);
			}
		}

		// Now solve u * x = y
		for (int kb = 0; kb < n; ++kb)
		{
			k = n - (kb + 1);
			b[k] /= a[k][k];
			t = -b[k];
			daxpy(k, t, &a[k][0], 0, b);
		}
	}
}

// Multiply matrix m times vector x and add the result to vector y
void dmxpy(int n1, double *y, int n2, int ldm, double *x, double **m)
{
	for (int j = 0; j < n2; ++j)
	for (int i = 0; i < n1; ++i)
		y[i] += x[j] * m[j][i];
}

// Estimates roundoff in quantities of size x
double epslon(double x)
{
	double eps = 0.0;
	double a = 4.0 / 3.0;
	double b, c;

	while (eps == 0)
	{
		b = a - 1.0;
		c = b + b + b;
		eps = abs(c - 1.0);
	}

	return eps * abs(x);
}

// Initialises the system
void initialise(double **a, double *b, double &ops, double &norma, double lda)
{
	long long nl = static_cast<long long>(SIZE);
	ops = (2.0 * static_cast<double>((nl * nl * nl))) / 3.0 + 2.0 * static_cast<double>((nl * nl));

	auto start = system_clock::now();
	norma = matgen(a, SIZE, b);
	auto end = system_clock::now();

}

// Runs the benchmark
void run(double **a, double *b, int &info, double lda, int n, int *ipvt)
{
	info = dgefa(a, n, ipvt);
	dgesl(a, lda, n, ipvt, b, 0);
}

// Validates the result
void validate(double **a, double *b, double *x, double &norma, double &normx, double &resid, double lda, int n)
{
	double eps, residn;
	double ref[] = { 6.0, 12.0, 20.0 };

	for (int i = 0; i < n; ++i)
		x[i] = b[i];

	norma = matgen(a, n, b);

	for (int i = 0; i < n; ++i)
		b[i] = -b[i];

	dmxpy(n, b, n, lda, x, a);

	resid = 0.0;
	normx = 0.0;
	for (int i = 0; i < n; ++i)
	{
		resid = (resid > abs(b[i])) ? resid : abs(b[i]);
		normx = (normx > abs(x[i])) ? normx : abs(x[i]);
	}

	eps = epslon(1.0);
	residn = resid / (n * norma * normx * eps);
	if (residn > CHECK_VALUE)
	{
		cout << "Validation failed!" << endl;
		cout << "Computed Norm Res = " << residn << endl;
		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
	}
	else
	{
		cout << "Calculations are correct!" << endl;
		cout << "Computed Norm Res = " << residn << endl;
		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
	}
}

int main(int argc, char **argv)
{
	// Allocate data on the heap
	double **a = new double*[SIZE];
	for (int i = 0; i < SIZE; ++i)
		a[i] = new double[SIZE];
	double *b = new double[SIZE];
	double *x = new double[SIZE];
	int *ipvt = new int[SIZE];

	double ldaa = static_cast<double>(SIZE);
	double lda = ldaa + 1;
	double ops, norma, normx;
	double resid;
	int info;

	initialise(a, b, ops, norma, lda);
	run(a, b, info, lda, SIZE, ipvt);

	validate(a, b, x, norma, normx, resid, lda, SIZE);


	// Calculate the duration
	// Get that time in ms. Write to file

	// Free the memory
	for (int i = 0; i < SIZE; ++i)
		delete[] a[i];
	delete[] a;
	delete[] b;
	delete[] x;
	delete[] ipvt;

	return 0;
}
