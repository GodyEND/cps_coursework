#include <iostream>
#include <fstream>
#include <chrono>
#include <thread>
#include <vector>
#include <omp.h>

#include "original.cpp"
#include "omp.cpp"

using namespace std;
using namespace chrono;
//using namespace OMPSpace;
using namespace Original;

// Defines the data size to work on
const unsigned int SIZE = 1000;
// Used to validate the result.  This is related to the data size
const double CHECK_VALUE = 12.0;


// Multiply matrix m times vector x and add the result to vector y
void dmxpy(int n1, double *y, int n2, int ldm, double *x, double **m)
{
	for (int j = 0; j < n2; ++j)
		for (int i = 0; i < n1; ++i)
			y[i] += x[j] * m[j][i];
}

// Estimates roundoff in quantities of size x
double epslon(double x)
{
	double eps = 0.0;
	double a = 4.0 / 3.0;
	double b, c;

	while (eps == 0)
	{
		b = a - 1.0;
		c = b + b + b;
		eps = abs(c - 1.0);
	}

	return eps * abs(x);
}

// Initialises the system
void initialise(double **a, double *b, double &ops, double &norma)
{
	long long nl = static_cast<long long>(SIZE);
	ops = (2.0 * static_cast<double>((nl * nl * nl))) / 3.0 + 2.0 * static_cast<double>((nl * nl));

	norma = matgen(a, SIZE, b);
}

// Runs the benchmark
void run(double **a, double *b, int &info, double lda, int n, int *ipvt)
{
	info = dgefa(a, n, ipvt);
	dgesl(a, n, ipvt, b, 0);
}

// Validates the result
void validate(double **a, double *b, double *x, double &norma, double &normx, double &resid, double lda, int n)
{
	double eps, residn;
	double ref[] = { 6.0, 12.0, 20.0 };

	for (int i = 0; i < n; ++i)
		x[i] = b[i];

	norma = matgen(a, n, b);

	for (int i = 0; i < n; ++i)
		b[i] = -b[i];

	dmxpy(n, b, n, lda, x, a);
	resid = 0.0;
	normx = 0.0;
	for (int i = 0; i < n; ++i)
	{
		resid = (resid > abs(b[i])) ? resid : abs(b[i]);
		normx = (normx > abs(x[i])) ? normx : abs(x[i]);
	}

	eps = epslon(1.0);
	residn = resid / (n * norma * normx * eps);
	if (residn > CHECK_VALUE)
	{
		cout << "Validation failed!" << endl;
		cout << "Computed Norm Res = " << residn << endl;
		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
	}
	else
	{
		cout << "Calculations are correct!" << endl;
		cout << "Computed Norm Res = " << residn << endl;
		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
	}
}

int main(int argc, char **argv)
{
	// Allocate data on the heap
	double** a = new double*[SIZE];
	for (int i = 0; i < SIZE; ++i)
		a[i] = (double*)_aligned_malloc(SIZE * sizeof(double), 16); 
	double *b = new double[SIZE];
	double *x = new double[SIZE];
	int *ipvt = new int[SIZE];

	double ldaa = static_cast<double>(SIZE);
	double lda = ldaa + 1;
	double ops, norma, normx;
	double resid;
	int info;

	// Main application
	auto start = system_clock::now();
	initialise(a, b, ops, norma);
	run(a, b, info, lda, SIZE, ipvt);
	auto end = system_clock::now();
	auto total = end - start;
	cout << duration_cast<milliseconds>(total).count() << endl;

	validate(a, b, x, norma, normx, resid, lda, SIZE);
		
	// Free the memory
	for (int i = 0; i < SIZE; ++i)
		_aligned_free(a[i]);
	delete[] a;
	delete[] b;
	delete[] x;
	delete[] ipvt;

	return 0;
}
