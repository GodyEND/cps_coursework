//#include <iostream>
//#include <fstream>
//#include <chrono>
//#include <thread>
//#include <vector>
//#include <omp.h>
//#include <future>
//
//using namespace std;
//using namespace chrono;
//
//// NAMESPACE SIMD128
//// NAMESPACE SIMD256
//// NAMESPACE OPENMP (2/4/8/16)
//// NAMESPACE THREADS
//// NAMESPACE ORIGINAL
//
//// Defines the data size to work on
//const unsigned int SIZE = 1000;
//// Used to validate the result.  This is related to the data size
//const double CHECK_VALUE = 12.0;
//const int NUM_THREADS = 4;
//vector<thread> threads;
//vector<int> result;
//
//double matgen(double **a, int n, double *b)
//{
//	double norma = 0.0;
//	int init = 1325;
//
//	__m256d* dataB = (__m256d*)b;
//	
//	for (int i = 0; i < n; ++i)
//	{
//		for (int j = 0; j < n; ++j)
//		{
//			init = 3125 * init % 65536;
//			a[j][i] = (static_cast<double>(init)-32768.0) / 16384.0;
//			norma = (a[j][i] > norma) ? a[j][i] : norma;
//		}
//	}
//
//#pragma omp parallel for num_threads(2)
//	for (int j = 0; j < n;  j+=4)
//	{
//		dataB[j/4] = _mm256_set1_pd(0.0);
//		for (int i = 0; i < n; ++i)
//		{
//			b[j] += a[i][j];
//			b[j+1] += a[i][j+1];
//			b[j+2] += a[i][j+2];
//			b[j + 3] += a[i][j + 3];
//		}
//	}
//	return norma;
//}
//
//int idamax(int n, double *dx, int dx_off)
//{
//	double dmax;
//	int itemp = 0;
//
//	itemp = 0;
//	dmax = abs(dx[0 + dx_off]);
//	for (int i = 0; i < n; ++i)
//	{
//		if (abs(dx[i + dx_off]) > dmax)
//		{
//			itemp = i;
//			dmax = abs(dx[i + dx_off]);
//		}
//	}
//	return itemp + dx_off;
//}
//
//// Scales a vector by a constant
//void dscal(int n, double da, double *dx, int dx_off)
//{
//	const unsigned int blocks = 4;
//	int range = n / blocks;
//	int rmdr = n % blocks;
//	
//
//	int simd_off = dx_off / 4;
//	if (rmdr != 0) simd_off += 1;
//	
//	__m256d* dataX = (__m256d*)dx + simd_off;
//	auto mult = _mm256_set1_pd(da);
//
//	// Remainder Check
//	for (int i = 0; i < rmdr; ++i)
//		dx[dx_off + i] *= da;
//
//	for (int i = 0; i < range; ++i)
//		dataX[i] = _mm256_mul_pd(dataX[i], mult);
//}
//
//// Constant times a vector plus a vector
//void daxpySIMD(int n, double da, double *dx, int dx_off, double *dy)
//{	
//	const unsigned int blocks = 4;
//	int i;
//	// SIMD Range
//	int range = n / blocks;
//	int rmdr = n % blocks;
//
//	// SIMD Offset
//	int simd_off = dx_off / blocks;
//	// Update SIMD offset if there is remaining data
//	if (rmdr != 0) simd_off += 1;
//
//	// Stream data X [4]Blocks // Offset data by offset +1 if not evenly divided
//	__m256d* dataX = (__m256d*)dx + simd_off;
//	// Incoming data Y [4]Blocks of doubles 
//	__m256d* dataY = (__m256d*)dy + simd_off;
//	
//	// Create data set with components da
//	auto mult = _mm256_set1_pd(da);
//
//	// Remainder Check
//	for (i = 0; i < rmdr; ++i)
//		dy[dx_off + i] += da * dx[dx_off + i];
//
//	// SIMD Instructions
//	for (i = 0; i < range; ++i)
//		dataY[i] = _mm256_add_pd(dataY[i], _mm256_mul_pd(mult, dataX[i]));
//}
//
//void daxpy(int n, double da, double *dx, int dx_off, double *dy)
//{
//	// Original
//	for (int i = 0; i < n; ++i)
//		dy[i + dx_off] = dy[i + dx_off] + da * dx[i + dx_off];
//}
//
//// Performs Gaussian elimination with partial pivoting
//int dgefa(double **a, int n, int *ipvt)
//{
//	// Pointers to columns being worked on
//	double *col_k, *col_j;
//	double t;
//	int kp1, l;
//	int nm1 = n - 1;
//	int info = 0;
//
//	//ofstream simdData("simd.csv", ofstream::out);
//	if (nm1 >= 0)
//	{
//		for (int k = 0; k < nm1; ++k)
//		{
//			// Set pointer for col_k to relevant column in a
//			col_k = &a[k][0];
//
//			kp1 = k + 1;
//
//			// Find pivot index
//			l = idamax(n - k, col_k, k);
//
//			ipvt[k] = l;
//
//			// Zero pivot means that this column is already triangularized
//			if (col_k[l] != 0)
//			{
//				// Check if we need to interchange
//				t = col_k[l];
//
//				if (l != k)
//				{
//					col_k[l] = col_k[k];
//					col_k[k] = t;
//				}
//
//				// Compute multipliers
//				t = -1.0 / t;
//				dscal(n - kp1, t, col_k, kp1); // Future do n-kp2
//
//				int j;
//#pragma omp parallel num_threads(4) default(none) shared(j, a, kp1, n, l, k, col_k) private(col_j, t)
//#pragma omp for
//				// Row elimination with column indexing
//				for (j = kp1; j < n; ++j)
//				{
//					// Set pointer for col_j to relevant column in a
//					col_j = &a[j][0];
//
//					t = col_j[l];
//					if (l != k)
//					{
//						col_j[l] = col_j[k];
//						col_j[k] = t;
//					}
//					//auto start = system_clock::now();
//					daxpySIMD(n - kp1, t, col_k, kp1, col_j);
//					//auto end = system_clock::now();
//					//simdData << duration_cast<microseconds>(end-start).count() << endl;
//				}
//				//simdData.close();
//			}
//		}
//	}
//
//	ipvt[n - 1] = n - 1;
//	if (a[n - 1][n - 1] == 0)
//		info = n - 1;
//
//	return info;
//}
//
//// Solves the system a * x = b using the factors computed in dgeco or dgefa
//void dgesl(double **a, int n, int *ipvt, double *b, int job)
//{
//	double t;
//	int k, l, nm1, kp1;
//
//	nm1 = n - 1;
//
//	// Solve a * x = b.  First solve l * y = b
//	if (nm1 >= 1)
//	{
//		for (k = 0; k < nm1; ++k)
//		{
//			l = ipvt[k];
//			t = b[l];
//			if (l != k)
//			{
//				b[l] = b[k];
//				b[k] = t;
//			}
//			kp1 = k + 1;
//			daxpySIMD(n - kp1, t, &a[k][0], kp1, b);
//		}
//	}
//
//	// Now solve u * x = y
//	for (int kb = 0; kb < n; ++kb)
//	{
//		k = n - (kb + 1);
//		b[k] /= a[k][k];
//		t = -b[k];
//		daxpy(k, t, &a[k][0], 0, b);
//	}
//}
//
//// Multiply matrix m times vector x and add the result to vector y
//void dmxpy(int n, double *y, double *x, double **m)
//{
//	for (int j = 0; j < n; ++j)
//	for (int i = 0; i < n; ++i)
//		y[i] += x[j] * m[j][i];		
//}
//
//// Estimates roundoff in quantities of size x
//double epslon(double x)
//{
//	double eps = 0.0;
//	double a = 4.0 / 3.0;
//	double b, c;
//
//	while (eps == 0)
//	{
//		b = a - 1.0;
//		c = b + b + b;
//		eps = abs(c - 1.0);
//	}
//
//	return eps * abs(x);
//}
//
//// Initialises the system
//void initialise(double **a, double *b, double &ops, double &norma)
//{
//	long long nl = static_cast<long long>(SIZE);
//	ops = (2.0 * static_cast<double>((nl * nl * nl))) / 3.0 + 2.0 * static_cast<double>((nl * nl));
//
//	norma = matgen(a, SIZE, b);
//}
//
//// Runs the benchmark
//void run(double **a, double *b, int &info, double lda, int n, int *ipvt)
//{
//	info = dgefa(a, n, ipvt);
//	dgesl(a, n, ipvt, b, 0);
//}
//
//// Validates the result
//void validate(double **a, double *b, double *x, double &norma, double &normx, double &resid, double lda, int n)
//{
//	double eps, residn;
//	double ref[] = { 6.0, 12.0, 20.0 };
//
//	for (int i = 0; i < n; ++i)
//		x[i] = b[i];
//
//	norma = matgen(a, n, b);
//
//	for (int i = 0; i < n; ++i)
//		b[i] = -b[i];
//
//	dmxpy(n, b, x, a);
//	resid = 0.0;
//	normx = 0.0;
//	for (int i = 0; i < n; ++i)
//	{
//		resid = (resid > abs(b[i])) ? resid : abs(b[i]);
//		normx = (normx > abs(x[i])) ? normx : abs(x[i]);
//	}
//
//	eps = epslon(1.0);
//	residn = resid / (n * norma * normx * eps);
//	if (residn > CHECK_VALUE)
//	{
//		cout << "Validation failed!" << endl;
//		cout << "Computed Norm Res = " << residn << endl;
//		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
//	}
//	else
//	{
//		cout << "Calculations are correct!" << endl;
//		cout << "Computed Norm Res = " << residn << endl;
//		cout << "Reference Norm Res = " << CHECK_VALUE << endl;
//	}
//}
//
//int main(int argc, char **argv)
//{
//	// Allocate data on the heap
//	double** a = new double*[SIZE];
//	for (int i = 0; i < SIZE; ++i)
//		a[i] = (double*)_aligned_malloc(SIZE * sizeof(double), 32);
//	double *b = new double[SIZE];
//	double *x = new double[SIZE];
//	int *ipvt = new int[SIZE];
//
//	double ldaa = static_cast<double>(SIZE);
//	double lda = ldaa + 1;
//	double ops, norma, normx;
//	double resid;
//	int info;
//
//	auto start = system_clock::now();
//	initialise(a, b, ops, norma);
//	run(a, b, info, lda, SIZE, ipvt);
//	auto end = system_clock::now();
//	auto total = end - start;
//	cout << duration_cast<milliseconds>(total).count() << endl;
//	
//
//	validate(a, b, x, norma, normx, resid, lda, SIZE);
//
//	// Free the memory
//	for (int i = 0; i < SIZE; ++i)
//		_aligned_free(a[i]);
//	delete[] a;
//	delete[] b;
//	delete[] x;
//	delete[] ipvt;
//
//	return 0;
//}
